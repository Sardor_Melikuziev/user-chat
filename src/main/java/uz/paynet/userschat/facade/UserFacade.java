package uz.paynet.userschat.facade;

import org.springframework.stereotype.Component;
import uz.paynet.userschat.dto.UserDTO;
import uz.paynet.userschat.entity.User;

@Component
public class UserFacade {

    public UserDTO userToUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstname(user.getName());
        userDTO.setLastname(user.getLastname());
        userDTO.setUsername(user.getUsername());
        return userDTO;
    }
}
