package uz.paynet.userschat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import uz.paynet.userschat.dto.MessageDTO;
import uz.paynet.userschat.entity.Message;
import uz.paynet.userschat.facade.MessageFacade;
import uz.paynet.userschat.payload.request.MessageRequest;
import uz.paynet.userschat.payload.response.MessageResponse;
import uz.paynet.userschat.services.MessageService;
import uz.paynet.userschat.validations.ResponseErrorValidation;

import javax.validation.Valid;
import java.security.Principal;

@CrossOrigin
@RestController
@RequestMapping("/api/message")
@PreAuthorize("permitAll()")
public class MessageController {

    @Autowired
    private MessageService messageService;
    @Autowired
    private ResponseErrorValidation responseErrorValidation;
    @Autowired
    private MessageFacade messageFacade;

    @PostMapping("/create")
    public ResponseEntity<Object> createMessage(@Valid @RequestBody MessageRequest messageRequest, Principal principal, BindingResult bindingResult) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        Message message = messageService.createMessage(messageRequest, principal);
        MessageDTO createdMessage = messageFacade.messageToMessageDTO(message);
        return new ResponseEntity<>(createdMessage, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<Object> updateMessage(@Valid @RequestBody MessageDTO messageDTO, BindingResult bindingResult) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        MessageResponse messageResponse = messageService.updateMessage(messageDTO);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageResponse> deleteMessage(@RequestParam("id") Long id) {
        messageService.deleteMessage(id);
        return new ResponseEntity<>(new MessageResponse("Message deleted successfully"), HttpStatus.OK);
    }
}
