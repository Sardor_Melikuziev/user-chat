package uz.paynet.userschat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsersChatApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsersChatApplication.class, args);
    }

}
