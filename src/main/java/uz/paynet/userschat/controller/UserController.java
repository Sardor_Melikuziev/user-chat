package uz.paynet.userschat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import uz.paynet.userschat.dto.UserDTO;
import uz.paynet.userschat.entity.User;
import uz.paynet.userschat.facade.UserFacade;
import uz.paynet.userschat.services.UserService;
import uz.paynet.userschat.validations.ResponseErrorValidation;

import javax.validation.Valid;
import java.security.Principal;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
@PreAuthorize("permitAll()")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserFacade userFacade;
    @Autowired
    private ResponseErrorValidation responseErrorValidation;

    @GetMapping("/")
    public ResponseEntity<UserDTO> getCurrentUser(Principal principal) {
        User user = userService.getCurrentUser(principal);
        UserDTO currentUser = userFacade.userToUserDTO(user);
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }

    @GetMapping("/by/id")
    public ResponseEntity getUserById(@RequestParam("id") Long userId) {
        User user = userService.getUserById(userId);
        UserDTO userDTO = userFacade.userToUserDTO(user);
        return new ResponseEntity(userDTO, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<Object> updateUser(@Valid @RequestBody UserDTO userDTO, BindingResult bindingResult, Principal principal) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        User user = userService.updateUser(userDTO, principal);
        UserDTO updatedUser = userFacade.userToUserDTO(user);
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }
}
