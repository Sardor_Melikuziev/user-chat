package uz.paynet.userschat.payload.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MessageRequest {

    @NotEmpty(message = "message should not be empty")
    private String message;
    private Long accUserId;
}
