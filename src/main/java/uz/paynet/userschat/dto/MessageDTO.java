package uz.paynet.userschat.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MessageDTO {

    private Long id;
    @NotEmpty(message = "message should not be empty")
    private String message;
    private Long accUserId;
}
