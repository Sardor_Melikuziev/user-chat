package uz.paynet.userschat.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class JWTTokenSuccessResponse {
    private boolean success;
    private String token;
}
