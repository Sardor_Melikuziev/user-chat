package uz.paynet.userschat.facade;

import org.springframework.stereotype.Component;
import uz.paynet.userschat.dto.MessageDTO;
import uz.paynet.userschat.entity.Message;

@Component
public class MessageFacade {
    public MessageDTO messageToMessageDTO(Message message) {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setId(message.getId());
        messageDTO.setMessage(message.getMessage());
        messageDTO.setAccUserId(message.getAccUserId());
        return messageDTO;
    }
}
