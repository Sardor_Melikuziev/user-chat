package uz.paynet.userschat.entity.enums;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
