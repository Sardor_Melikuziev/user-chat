package uz.paynet.userschat.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.paynet.userschat.dto.MessageDTO;
import uz.paynet.userschat.entity.Message;
import uz.paynet.userschat.entity.User;
import uz.paynet.userschat.payload.request.MessageRequest;
import uz.paynet.userschat.payload.response.MessageResponse;
import uz.paynet.userschat.repository.MessageRepository;
import uz.paynet.userschat.repository.UserRepository;

import java.security.Principal;
import java.util.List;

@Service
public class MessageService {

    public static final Logger LOG = LoggerFactory.getLogger(MessageService.class);

    private final MessageRepository messageRepository;
    private final UserRepository userRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository, UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    public Message createMessage(MessageRequest messageRequest, Principal principal) {
        User user = getUserByPrincipal(principal);
        Message message = new Message();
        message.setMessage(messageRequest.getMessage());
        message.setUser(user);
        message.setAccUserId(messageRequest.getAccUserId());
        LOG.info("message saved successfully!!!");
        return messageRepository.save(message);
    }

    public MessageResponse updateMessage(MessageDTO messageDTO) {
        List<Message> messages = messageRepository.findMessageById(messageDTO.getId());
        if (messages.size() != 0) {
            Message message = messages.get(0);
            message.setMessage(messageDTO.getMessage());
            messageRepository.save(message);
            return new MessageResponse("Edited");
        } else {
            return new MessageResponse("Message did not found");
        }
    }

    public void deleteMessage(Long messageId) {
        messageRepository.deleteById(messageId);
        LOG.info("Message Deleted with id: " + messageId);
    }

    private User getUserByPrincipal(Principal principal) {
        String username = principal.getName();
        return userRepository.findUserByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found with username " + username));
    }
}
